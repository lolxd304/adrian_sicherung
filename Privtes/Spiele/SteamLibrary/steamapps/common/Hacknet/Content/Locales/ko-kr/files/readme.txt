﻿﻿Readme.txt

식료품 시뮬레이터 2011을 구입해 주셔서 감사합니다.

식료품 시뮬레이터 2011에서 플레이어는 다섯 가지 다른 캐릭터 클래스 중 하나에서 영웅의 역할을 하도록 관리해야 합니다. 플레이어는 자신의 캐릭터 수준을 높이고 좋은 아이템을 받기 위해 야생 지역과 던전에서 몬스터와 싸웁니다. 전투는 실시간이고, 등각 관점에서 보입니다. 플레이어는 또한 자신을 따르고 근처의 적들을 공격하는 여러 컴퓨터 제어 용병 또는 고용인들 중 한 명을 고용하는 옵션이 있습니다. 강력한 보스 몬스터가 각 행위의 마지막에서 플레이어를 기다립니다. 아이템 드롭, 몬스터 속성 및 대부분의 던전 레이아웃은 디아블로 II에 의해 무작위로 생성됩니다.

이전 둠 게임처럼, 게임의 목적은 옆으로 각각 이동하고, 90도 단위로 회전을 하여 간극 없이 열 개 블록의 수평 라인을 만드는 것입니다. 그러나, 둠 3의 더 많은 이야기 중심의 접근 방식은 플레이어가 키 플롯 정보, 목표 및 파워 스타를 제공하는 아군의 비 플레이어 캐릭터를 종종 발견한다는 것을 의미합니다.[7] 각각의 겔럭시에는 플레이어가 탐험할 수 있는 많은 행성과 기타 공간 물질이 있습니다.[7] 게임은 독특한 기능을 허용하는 새로운 물리 시스템을 사용하는데, 플레이어나 물체를 공기 속으로 때로는 스웨덴 최대의 아마추어 민속 문화 조직이라고 보통 불리는 Folkdansringen, 즉 Svenska Folkdansringen (스웨덴 민속 댄스 링)으로 보내버리는 에어리얼 페이스 플레이트가 있습니다.