Key (Optional)	English	Korean	Description
	config.txt	config.txt	filename for a config file
	stash	stash	filename
	misc	misc	filename
	Failed	실패	
	Successful	성공	
	Complete	완료	
	Back	돌아가기	
	Retry	재시도	
	Login	로그인	
	User	유저이름	Short for Username
	Pass	비밀번호	Short for Password
	Cancel	취소	
	Connected to	에 연결됨	
	You are the Administrator of this System	당신은 이 시스템의 관리자입니다	
	Probe System	Probe 시스템	Keep word Probe in english
	View Filesystem	파일 시스템 보기	
	View Logs	로그 보기	
	Scan Network	네트워크 Scan	Keep word scan in english
	Disconnect	Disconnect	
	Disconnected	접속 종료됨	
	Error	오류	
	Open Ports	포트 열기	
	Open Ports Required for Crack:	크랙에 필요한 포트 열기:	
	Proxy Detected	Proxy 탐색됨	Keep word Proxy in English if possible
	Proxy Bypassed	Proxy 우회됨	Keep word Proxy in English if possible
	Firewall Detected	Firewall 탐색됨	Keep word Firewall in English if possible
	Firewall Solved	Firewall 무력화됨	Keep word Firewall in English if possible
	File System	파일 시스템	
	Empty	비어있음	
	PASSWORD	비밀번호	
	FOUND	검색됨	
	New Session	신규 세션	
	Hacknet	Hacknet	Any way of writing this name in local characters? I'll likely keep it in english mostly.
	MORE SOON	MORE SOON	For the demo end screen
	Exit	종료	
	Back	돌아가기	
			
Less Critical Words:			
PointClicker:			
			
	Click Me!	클릭!	
	Autoclicker	오토클리커	Made up word -whatever fits
	Pointereiellion	Pointereiellion	
	Upgrade	업그레이드	
	IMPORTANT : NEVER DELETE OR RE-NAME	중요: 절대 지우거나 이름을 수정하지 마시오	
	IT IS SYSTEM CRITICAL!	이건 시스템 크리티컬이라고!	
	Removing it causes instant crash. DO NOT TEST THIS	이것을 삭제하면 인스턴트 크래시가 발생할 수 있습니다. 테스트하지 마시오.	
	IMPORTANT_README_DONT_CRASH.txt	IMPORTANT_README_DONT_CRASH.txt	
	UPGRADE AVALIABLE	사용 가능한 업데이트	
	INSUFFICIENT POINTS	포인트 부족	
	COST	비용	
	% of current Points	현재 포인트의 %	
	% of current Points Per Second	초당 현재 포인트의 %	
	CLICK TO UPGRADE	업데이트 하려면 클릭	
	INSUFFICIENT POINTS	포인트 부족	"as in ""5 seconds to double current points"""
	seconds to double current points	현재 포인트의 두 배까지 초 남음	
	GO!	시작!	
			
Medical:			
			
	UNKNOWN ERROR	알 수 없는 오류	
	Universal Medical Database	범용 의료 데이터베이스	
	MedicalDatabaseInfo.txt	MedicalDatabaseInfo.txt	
	No entry found for name	이름을 찾을 수 없습니다	permutations means varieties
	Permutations tested:	순열 테스트됨:	
	Corrupt record	손상된 기록	
	Unable to parse record	기록 분석 불가	
	MedicalRecord	의료기록	
	Back to menu	메뉴로 돌아가기	
	Send Record Copy	기록 사본 전송	"as in ""record for NAME"""
	Record for	기록	
	Recipient Address	수취인 주소	
	Recipient Address (Case Sensitive):	수취인 주소 (대소문자 구별):	
	Error getting recipient email	수취인 이메일 가져오기 오류	
	Specify Address	주소 지정	
	COMPLETE	완료	
	SENDING	전송 중	
	Send to different address	다른 주소로 보내기	
	Back to menu	메뉴로 돌아가기	
	Info	정보	
	e-mail this record	이 기록을 이메일로 전송	
	Age	나이	
	Years	년	
	Days	일	
	Hours	시간	
	Minutes	분	
	Seconds	초	
	Universal Medical	범용 의료	
	Records & Monitoring Services	기록 및 모니터링 서비스	
	Information	정보	
	Details and Administration	자세한 정보 및 관리	
	Database	데이터베이스	
	Records Lookup	기록 열람	
	Search	검색	
	Random Entry	랜덤 검색	
	Enter patient name	환자 이름 입력	
	Error in name input	이름 입력 오류	
	Exit Database View	데이터베이스 보기 종료	
	Admin Access	관리자 접속	
	Required for use	사용 필요	
			
Academic:			
			
	DESCRIPTION FILE NOT FOUND	설명 파일이 없습니다	
	Valid Administrator Account Detected	인증된 관리자 계정 감지됨	
	Non-Admin Account Active	비관리자 계정 활성화	
	About This Server	이 서버에 대하여	
	Search Entries	검색 결과	
	Search Again	다시 검색	
	No Entries Found	검색 결과 없음	
	Information	정보	
	Please enter the name you with to search for:	검색하려는 이름을 입력하세요:	
	Name	이름	
	Multiple Matches	다중 매치	
	Refine Search	상세 검색	
	Go Back	되돌아가기	
	No Degrees Found	검색된 학위 없음	
	Degree	학위	
	Uni	대학교	Short term for University
	Edit	수정	
	Delete	삭제	
	Confirm?	확인하시겠습니까?	
	Add Degree	학위 추가	
	Save And Return	저장 후 돌아가기	
	International Academic Database	국제 학술 데이터베이스	
			
			
Authentication:			
	username :	유저이름:	
	password :	비밀번호:	
			
			
Death Row:			
	Return	돌아가기	
	Incident Report	사고 보고서	
	Final Statement	최후 진술	
			
			
Heart Monitor:			
	Admin Access Granted	관리자 접속 승인	
	Admin Access Required	관리자 접속 필요	
	View Monitor	모니터 보기	
	Admin Login	관리자 로그인	
	Firmware	펌웨어	
	Monitor	모니터	
	Firmware Config	펌웨어 Config	
	Firmware Administration	펌웨어 관리	
	Access	접속	
	GRANTED	승인됨	
	DENIED	거절됨	
	Secondary security layer for firmware read/write access	펌웨어 쓰기/읽기 권한을 위한 보조 안전 레이어	
	Access Required	접속 요구됨	
	Log In First	첫 번째 로그인	
	Currently Active Firmware	현재 활성화된 펌웨어	
	Activate This Firmware	이 펌웨어 활성화	
	Confirm Firmware Activation	펌웨어 활성화 확인	
	Confirm Activation	활성화 확인	
	FIRMWARE UPDATE COMPLETE	펌웨어 업데이트 완료	
	Valid Firmware File	유효한 펌웨어 파일	
	Invalid Firmware File	유효하지 않은 펌웨어 파일	
	Invalid binary package	유효하지 않은 바이너리 패키지	1
	Valid firmware packages must be	유효한 펌웨어 패키지여야 함	2
	digitally signed by an authorized manufacturer	인증된 발행자에 의해 디지털 서명됨	3
	Valid binary package	유효한 바이너리 패키지	
	Signed by	에 의해 서명됨	
	Compiled by	에 의해 컴파일됨	
	"A secondary login is required to review and modify running firmware. Personal login details are provided for each chip. If you have lost your login details, connect your support program to our content server"	"작동 중인 펌웨어 리뷰 및 수정을 위해서는 보조 로그인이 필요합니다. 개인 로그인 정보는 각 칩에 전송됩니다. 로그인 정보를 잃어버렸을 경우, 서포트 프로그램을 컨텐트 서버에 연결하십시오."	
	Login Complete	로그인 완료	
	Login Failed	로그인 실패	
	Retry Login	로그인 재시도	
	Administrate Firmware	펌웨어 관리	
	Username	유저이름	
	Password	비밀번호	
	Remote Monitor	원격 모니터	
			
ISP:			
	ISP Management System	ISP 관리 시스템	
	IP Entry	IP 엔트리	
	Identified as	확인됨	
	Assign New IP	새 IP 할당	
	Flag for Inspection	검사 플래그	
	ACTIVE	활성	
	Prioritize Routing	라우팅 우선 순위 결정	
	Scanning for	Scan 중	
	Enter IP Address to Scan for	Scan할 IP 주소 입력	
	IP Address	IP 주소	
	Scan	Scan	
	Insufficient Permissions	권한 없음	
	IP Address is not registered with	에 IP 주소가 등록되어 있지 않습니다	
	our servers	우리 서버	
	Check address and try again	주소를 확인하고 다시 시도하세요	
	About this server	이 서버에 대하여	
	Valid Administrator Account Detected	유효한 관리자 계정 탐지됨	
	Non-Admin Account Active	비관리자 계정 활성화	
	About	대하여	
	Search for IP	IP 검색	
			
			
Mail Server:			
	Mail Server	메일 서버	
	Return to Inbox	받은 편지함으로 돌아가기	
	Subject	제목	As in Email Subject line
	LINK	링크	
	ACCOUNT	계정	
	NOTE	NOTE	
	Reply	답장	
	Logged in as {0}	{0} 계정으로 로그인	Where {0} is the username of the logged in user
	Logout	로그아웃	
	Return to Inbox	받은 편지함으로 돌아가기	
	Additional Details	추가 세부 사항	
	Force Complete	포스 완료	
	Mission Incomplete	미션 완료	
	Add	추가	
	Send	전송	
			
			
Message Board:			
	Digital Security	디지털 보안	
	Message Board	메시지 게시판	
	Back to Board	게시판으로 돌아가기	
	{0} posts and image replies omitted	{0} 개의 포스트와 이미지 답변이 누락됨	Where {0} is the number of posts
	Click here to view.	보려면 이곳을 클릭	
	Anonymous	익명	
			
			
Mission Hub:			
	Contract Listing	계약 목록	
	User List	유저 리스트	
	Abort Current Contract	현재 계약 종료	
	Are you sure you with to abandon your current contract?	현재 계약을 종료하시겠습니까?	
	This cannot be reversed.	이것은 취소할 수 없습니다.	
	Abandon Contract	계약 종료	
	CSEC secure contract listing panel : Verified Connection : Token last verified	CSEC 확실한 계약 리스트 패널: 인증된 연결: 토큰 최종 인증됨	
	Abort current contract to accept new ones.	새 계약을 위해 현재 계약 종료	
	Joined {0}	{0} 일에 가입함	{0} is a date
	Previous Page	이전 페이지	
	Next Page	다음 페이지	
	{0} Contract Hub	{0} 계약 허브	"{0} is a group name like ""CSEC"""
	Authenticated User:	인증된 사용자:	
			
			
Mission Listing:			
	{0} News	{0} 뉴스	"{0} is the company name - like ""fox news"""
	Available Contracts	가능한 계약	
	CRITICAL ERROR	심각한 오류	
	{0} Group	{0} 그룹	{0} as above
	Message Board	메시지 보드	
	Mission Unavailable	사용할 수 없는 미션	
	Complete Existing Contracts	현재 계약 완료	"As an instruction: ""You need to complete existing contracts"""
	User ID Assigned to Different Faction	다른 팩션에 할당된 유저 ID	
	Mission Unavailable	사용할 수 없는 미션	
			
			
Song Changer:			
	Music Changer	음악 체인저	
	Shuffle Music	무작위 음악	
			
			
Web Server:			
	Exit Web View	웹 보기 종료	
	Error 404	Error 404	
	Page not found	페이지를 찾을 수 없습니다	
	View Source	소스 보기	
	Loading...	로딩...	
			
			
Effects:			
			
Trace Danger Seq:			
	IP Address successfully reset	IP 주소가 성공적으로 리셋되었습니다	
	Foreign trace averted	외부 추적 방지됨	
	Preparing for system reboot	시스템 재시작 준비 중	
	Rebooting	재시작	
	COMPLETED TRACE DETECTED : EMERGENCY RECOVERY MODE ACTIVE	완료된 추적 감지됨: 긴급 복구 모드 활성화	
			
	Unsyndicated foreign connection detected during active trace	활성 추적 중에 언신디케이트 외부 접속이 감지되었습니다	Unsyndicated is a technical term - leave as is or find close equivilent
	Emergency recovery mode activated	긴급 복구 모드가 활성화되었습니다	Following terms flash in in sequence
	Automated screening procedures will divert incoming connections temporarily	자동화 스크리닝 절차가 들어오는 연결을 임시로 우회합니다	
	This window is a final opportunity to regain anonymity.	이 창은 익명을 회복할 수 있는 마지막 기회입니다.	
	"As your current IP Address is known, it must be changed"	"당신의 IP 주소가 알려져있기 때문에, 반드시 이를 변경해야 합니다"	
	This can only be done on your currently active ISP's routing server	아이피 변경은 현재 활성화된 ISP 라우팅 서버에서만 할 수 있습니다	
	Reverse tracerouting has located this ISP server's IP address as {0}	역방향 경로추적으로 이 ISP 서버의 IP 주소 {0}를 찾았습니다	{0} being an ip address
	Your local ip : {0}  must be tracked here and changed.	당신의 로컬 ip : {0} 는 추적 및 변경되어야 합니다.	
	Failure to complete this while active diversion holds will result in complete	활동 전환이 보류되는 동안 완료하지 못할 경우 모든 데이터는	
	and permanent loss of all account data - THIS IS NOT REPEATABLE AND CANNOT BE DELAYED	영구적으로 완전히 손실됩니다 - 이것은 반복할 수 없으며 미룰 수 없습니다	
			Sequence ends
	Reset Assigned Ip Address on ISP Mainframe	ISP 메인프레임의 할당된 IP 주소 리셋	
	ISP Mainframe IP:   {0}	ISP 메인프레임 IP:  {0}	
	YOUR Assigned IP: {0}	당신의 할당된 IP: {0}	
	BEGIN	시작	
			
Exes:			
	Hint	힌트	
			
Decypher:			
	Fatal error in decryption	심각한 암호 해독 오류	
	file may be corrupt	파일이 손상되었을 수 있습니다	
	File not found	파일을 찾을 수 없습니다	
	File is not	파일이 아닙니다	
	DEC encrypted	DEC 암호화	
	This File Requires\n a Password	이 파일을 열려면 \n 암호가 필요합니다	keep a \n in the middle of the text
	Provided Password\nIs Incorrect	입력한 비밀번호가 \n 올바르지 않습니다	as above
	Fatal error in loading	심각한 로딩 오류	
	COMPLETE	완료	
	ERROR	오류	
	Headers:	제목	
	Source IP:	소스 IP:	
	Output File:	출력 파일:	
	Operation Complete	작동 완료	
	Shutting Down	셧다운 중	
			
			
Notes:			
	Close	종료	
	Add Note	Note 추가	
	Type In Terminal...	터미널 입력...	
	Insufficient Memory	메모리 부족	
	Close Notes to Free Space	공간 확보를 위해 Note 닫기	
			
			
Sequencer:			
	ACTIVATE	활성화	
	Break active security on target	타켓의 활성 보안 무력화	
	Delete all Hacknet related files	Hacknet 관련 파일 삭제	
	Disconnect	Disconnect	
			
			
Shell:			
	Trigger	트리거	
	Close	종료	
	Overload	Overload	
	Trap	트랩	
	Cancel	취소	
			
			
ThemeChanger:			
	Theme Switch	테마 변경	
	Remote	원격	
	No Valid Files	유효한 파일 없음	
	Local Theme Files	로컬 테마 파일	
	Activate Theme	테마 활성화	
			A title for the player - as in Hacker Agent
			
CSEC:			
	Agent	에이전트	
	"Additional resources have been added to the CSEC members asset pool, for your free use."	"자유롭게 이용할 수 있도록, 추가 리소스가 CSEC 멤버 자산 명단에 추가되었습니다."	
	Find them in the misc folder on the asset server.	에셋 서버의 음악 폴더에서 찾아보세요.	
	Thankyou	고마워	
	Admins :: Asset Uploads	관리자 :: 에셋 업로드	
	Admins :: Flagged for Critical Contract	관리자 :: 중요 계약 플래그 지정	
			
			
Base Faction:			
	Contract Abandoned	계약 포기됨	
			
			
MODULES:			
DisplayModule:			
	Empty	비어있음	
			
Crash Module:			
	Unable to Load System file os-config.sys	시스템 파일 os-config.sys를 로드할 수 없음	
	Unable to Load System file bootcfg.dll	시스템 파일 bootcfg.dll을 로드할 수 없음	
	Unable to Load System file netcfgx.dll	시스템 파일 netcfgx.dll을 로드할 수 없음	
	Unable to Load System file x-server.sys	시스템 파일 x-server.sys를 로드할 수 없음	
	Locate and restore a valid x-server file in ~/sys/ folder to restore UX functionality	UX 기능을 복구하기 위해 ~/sys/ 폴더 내 유효한 x-server 파일을 찾아 복구하십시오	
	Consider examining reports in ~/log/ for problem cause and source	문제 원인으로 ~/log/ 내의 리포트 검사를 고려하십시오	
	System UX resources unavailable -- defaulting to terminal mode	시스템 UX 리소스 이용 불가 - 터미널 모드 디폴트 설정	
			
RAM Module:			
	INSUFFICIENT MEMORY	메모리 부족	
			
			
Programs:			
	No port number Provided	포트 넘버가 제공되지 않음	
	Target Port is Closed	타겟 포트가 닫혀있습니다	
	Target Port running incompatible service for this executable	타겟 포트가 실행파일과 호환되지 않는 서비스 실행 중	
			
Firewalls:			
	Too few characters	글자가 너무 적음	
	Too many characters	글자가 너무 많음	
	Solution Incorrect Length	잘못된 길이의 솔루션	
	Analysis already in Progress	이미 분석 작업 진행 중	
	Firewall Analysis Pass {0}	Firewall 분석 패스 {0}	"{0} is the pass number (pass 2, pass 3 etc)"
			
			
Utilities:			
	Search	검색	
	Cancel	취소	
			
			
Savefile Login:			
	USERNAME	유저이름	
	PASSWORD	비밀번호	
	CONFIRM PASS	패스 확인	
	Username already in use. Try Again	이미 사용 중인 유저 이름입니다. 다시 입력하세요	
	Password Cannot be Blank! Try Again	비밀번호는 비워둘 수 없습니다! 다시 입력하세요	
	Password Mismatch! Try Again	비밀번호가 맞지 않습니다! 다시 입력하세요	
	Details Confirmed	정보 확인됨	
	READY - PRESS ENTER TO CONFIRM	준비 - 확인하려면 엔터를 누르세요	
	Invalid Login Details	유효하지 않은 로그인 정보	
	LOCAL ACCOUNTS	로컬 계정	
	CONFIRM	확인	
	CANCEL	취소	
			
Helpfile:			NOTE: Be very careful translating these ones - they are for the help command ingame and provide instruction to the player
	Command List - Page [PAGENUM] of [TOTALPAGES]	명령어 리스트 -  [TOTALPAGES]의 [PAGENUM] 페이지	Leave [Pagenum] and [totalpages] untranslated - they are replaced by code
	Displays the specified page of commands.	지정된 명령어 페이지 표시	
	Copies file named [filename] from remote machine to specified local folder (/bin default)	원격 머신에서 지정된 로컬 폴더로 [filename] 파일을 복사 (/bin default)	
	Scans for links on the connected machine and adds them to the Map	연결된 머신의 링크 Scan 및 맵에 추가	
	Deletes specified file(s)	지정된 파일(들) 삭제	
	Lists currently running processes and their PIDs	현재 실행 프로세스 및 PIDs 리스트	"Leave ""PIDs"" as-is"
	Moves current working directory to the specified folder	현재 워킹 디렉토리를 지정된 폴더로 옮김	
	Moves or renames [FILE] to [DESTINATION]	[FILE]을 [DESTINATION]로 옮기거나 이름 바꾸기	
	Connect to an External Computer	외부 컴퓨터로 연결	
	Scans the connected machine for{0}active ports and security level	활성 포트에 연결된 머신 {0} 및 보안 레벨 Scan	{0} is a splitter in the middle of the text so it's not too long as a line here.
	Lists all available executables in the local /bin/ folder (Includes hidden and embedded executables)	로컬 /bin/ 폴더 내 사용 가능한 실행 파일 리스트 (숨겨진 혹은 임베디드된 실행 파일 포함)	
	Terminate the current open connection.	현재 열린 연결 종료	
	Displays contents of file	파일 컨텐츠 표시	
	Opens the connected Computer's CD Tray	연결된 컴퓨터 CD 트레이 열기	
	Closes the connected Computer's CD Tray	연결된 컴퓨터 CD 트레이 종료	
	Reboots the connected computer. The -i flag reboots instantly	연결된 컴퓨터 재시작. 즉시 -i 플래그 재시작	
	Replaces the target text in the file with the replacement	파일 내 타겟 텍스트를 대체재로 교체	
	Performs an analysis pass on the firewall of the target machine	타겟 머신의 Firewall 패스 분석 실행	
	Attempts to solve the firewall of target machine to allow UDP Traffic	UDP 트래픽 허가하도록 타겟 머신의 Firewall 무력화 시도	
	Requests a username and password to log in to the connected system	연결된 시스템 로그인 비밀번호 및 유저이름 요청	
	Uploads the indicated file on your local machine to the current connected directory	로컬 머신에서 현재 연결된 디렉토리로 표시된 파일 업로드	
	Clears the terminal	터미널 화면 지움	
			
			
			
Main Menu:			
	TEST BUILD EXPIRED - EXECUTION DISABLED	테스트 빌드 만료됨 - 실행 불가	
	New Session	신규 세션	
	OS Load Error	OS 로드 오류	
	Load Session	세션 로드	
	New Test Session	신규 테스트 세션	
	Continue with account [{0}]	[{0}] 계정으로 계속	{0} is the account name - keep the brackets
	No Accounts	계정 없음	
	Settings	세팅	
	"ACCOUNT FILE CORRUPTION: Account {0} appears to be corrupted, and will not load."	계정 파일 손상: {0} 계정이 손상되어 로드되지 않습니다.	
	WARNING: Error connecting to Steam Cloud	경고: 스팀 클라우드 연결 오류	
			
			
			
Messages:			
	Resume	계속하기	
	Quit Hacknet	Hacknet 종료	
	Quit HackNetOS\nVirtual Machine?	HackNetOS 버추얼 머신을 \n 종료하시겠습니까?	Leave the \n in the middle there
			
			
Mission Functions:			
	Contract Successful	계약 성공	
	Are you Kidding me?	지금 장난해?	
	eOS Security Basics	eOS 보안 기초	
			
			
Multiplayer Old:			
	Connection Established	연결 설정됨	
	Local IP	로컬 IP	
	Extrn IP	외부 참조 IP	
	VICTORY	승리	
	DEFEAT	패배	
	Connecting...	연결 중...	
	IP To Connect to:	IP 연결:	
	Back to Menu	메뉴로 가기	
			
Settings:			
	Back	돌아가기	
	Apply Changes	변경사항 저장	
	Resolutions	해상도	
	Fullscreen	풀스크린	
	Bloom	블룸	
	Scanlines	스캔라인	
	Multisampling	멀티샘플링	
	Sound Enabled	사운드 끄기	
	Music Volume	음악 볼륨	
	Text Size	텍스트 크기	
			
			
OS:			
	Location	위치	
	Home IP	홈 IP	
	Loading...	로딩 중...	
	Program not Found	프로그램을 찾을 수 없음	
	Proxy Active -- Cannot Execute	Proxy 활성화 -- 실행 불가	
	Insufficient Memory	메모리 부족	
	Location: Not Connected	위치: 연결되지 않음	
	Target Machine Rejecting Syndicated UDP Traffic	타겟 머신이 UDP 트래픽 신디케이트 거절	
	Bypass Firewall to allow unrestricted traffic	제한되지 않은 트래픽 허가하도록 Firewall 우회	
	Too Few Open Ports to Run	실행하기엔 열린 포트가 너무 적습니다	
	Open Additional Ports on Target Machine	타겟 머신에 추가 포트 열기	
	Requires Administrator Access to Run	실행하려면 관리자 접속이 필요합니다	
	This computer is already running a shell.	이 컴퓨터는 이미 shell을 실행 중입니다.	
			
			
Programs:			
	BOOT ERRORS DETECTED	부팅 오류 감지됨	
	"For A Command List, type \""help\"""	"명령어 리스트를 보려면, \""help\""를 입력하세요"	leave a backslash before any quote mark
	Launching Tutorial...	튜토리얼 불러오는 중...	
	Invalid PID	유효하지 않은 PID	A PID is a type of identifier number
	Error: Invalid PID or Input Format	오류: 유효하지 않은 PID 혹은 입력 포맷 	
			
			
			
			
Future:			
	Extensions	확장팩	
	Mods	모드	
	Load {0} Session	{0} 세션 로드	{0} being a mod name
	Install Extensions via Steam Workshop	스팀 워크샵을 통해 확장팩 설치	
	No Extensions Found	검색된 확장팩 없음	
	Workshop	워크샵	
	Saved Sessions	세션 저장됨	
	Creator:	만든 이:	
			
			
			
			
Localization:			
	Language	언어	
	Select Language	언어 설정	
	WARNING	경고	
	"Once created, a session's language cannot be changed"	"일단 한번 설정하면, 세선의 언어는 변경할 수 없습니다"	
	Active Language:	활성 언어:	
	Activate	활성화	
	Back to Extension List	확장팩 리스트로 돌아가기	
			
			
			
Extra:			
	Are you sure you wish to delete account {0}?	계정 {0}를 정말 삭제하시겠습니까?	
	Delete Account	계정 삭제	
	Cancel	취소	
	Invalid Last Account : Login Manually	유효하지 않은 최근 계정: 수동으로 로그인하십시오	
	Audio Visualiser	오디오 비주얼라이저	
			
			
			
			
Extra v1:			
	Exit to Menu	메뉴로 가기	
	Quit HackNetOS\nCurrent Session?	HackNetOS 현재 세션을 \n 종료하시겠습니까?	
	Username cannot be blank. Try Again	유저 이름은 비워둘 수 없습니다. 다시 입력하세요	
			
Extra:			
	Are you sure you wish to delete account {0}?	어카운트 {0}를 정말 삭제하시겠습니까?	
	Delete Account	어카운트 삭제	
	Cancel	취소	
	Invalid Last Account : Login Manually	유효하지 않은 최근 어카운트: 수동 로그인	
	Audio Visualiser	오디오 비주얼라이저	
			
			
			
			
Extra v1:			
	Exit to Menu	메뉴로 가기	
	Quit HackNetOS\nCurrent Session?	HackNetOS\n현재 세션을 종료하시겠습니까?	
	Username cannot be blank. Try Again	유저이름은 빈칸으로 둘 수 없습니다. 다시 시도하세요	
	Continue	계속	
	"For A Command List, type ""help"""	"커맨드 리스트를 보려면, ""help""를 입력하세요"	
	IP Search is limited to administrators	"IP 검색은, 이 머신의"	
	of this machine only	관리자만 가능합니다	
	"If you require access, contact customer support"	"접근 권한이 필요하면, 고객 센터에 연락하세요."	
	"A secondary login is required to review and modify running firmware. Personal login details are provided for each chip. If you have lost your login details, connect your support program to our content server"	"작동 중인 펌웨어 리뷰 및 수정을 위해서는 보조 로그인이 필요합니다. 개인 로그인 정보는 각 칩에 전송됩니다. 로그인 정보를 잃어버렸을 경우, 서포트 프로그램을 컨텐트 서버에 연결하십시오."	
	Accept	수락	
	"eOS Device \{0}\"" opened for connection at {1}"""	"""{1} 연결을 위해 "" eOS 디바이스 \{0}\가 열렸습니다"	
	SCANNING	스캐닝	
	ERROR	오류	
	SCAN COMPLETE	스캔 완료	
	DEVICES FOUND : {0}	검색된 디바이스: {0}	
	ADMIN ACCESS\nREQUIRED FOR SCAN	스캔하려면 \n관리자 권한 필요	
	Mission Incomplete	미션 실패	
	default	디폴트	
	medium	미디움	
	large	라지	
	Home IP	홈 IP	
			
	Kills Process number [PID]	넘버 [PID] 프로세스 강제 종료	
	Lists all files in current directory	현재 디렉토리의 모든 파일 리스트	
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
