﻿
-- Theme Changer: LiesMich --

Dieses Programm erlaubt das Austauschen von x-server Designs von lokalen oder verbundenen Systemen.
Dateien, die unter "Im Netzwerk" aufgeführt werden, befinden sich im aktiven Verzeichnis einer verbundenen Maschine.
Dateien, die unter "Lokal" aufgeführt sind, befinden sich auf der eigenen Maschine. (im ~/home oder ~/sys Verzeichnis).
ThemeChanger erlaubt die Auswahl einer beliebigen Theme-Datei, welche daraufhin automatisch die entsprechenden Informationen 
in die x-server.sys runterlädt und den Theme aktiviert - ohne, dass ein Neustart benötigt wird.
Außerdem erstellt das Programm automatisch Backups von bereits genutzten Themes, um sie für
spätere Verwendung bereit zu halten.