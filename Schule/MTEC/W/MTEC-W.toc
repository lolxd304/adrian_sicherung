\select@language {german}
\contentsline {part}{\numberline {I}Spannende Verfahren}{5}
\contentsline {chapter}{\numberline {1}Allgemein}{7}
\contentsline {section}{\numberline {1.1}Einflussfaktoren des Spannens}{7}
\contentsline {section}{\numberline {1.2}Die Werkzeugschneide}{7}
\contentsline {subsection}{\numberline {1.2.1}Die Winkel am Schneidteil}{7}
\contentsline {section}{\numberline {1.3}Die Bewegungen zwischen Werkzeug und Werkst\IeC {\"u}ck}{8}
\contentsline {section}{\numberline {1.4}Die Spannbildung}{8}
\contentsline {section}{\numberline {1.5}Die Schneidstoffe (Der Werkstoff f\IeC {\"u}r den Schneidkeil)}{8}
\contentsline {subsection}{\numberline {1.5.1}Eigenschaften}{8}
\contentsline {chapter}{\numberline {2}Die Spannende Formgebung}{9}
\contentsline {section}{\numberline {2.1}Von Hand}{9}
\contentsline {subsection}{\numberline {2.1.1}Feilen}{9}
