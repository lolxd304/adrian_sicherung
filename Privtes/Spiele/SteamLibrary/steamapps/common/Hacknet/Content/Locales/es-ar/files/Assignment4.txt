Clase de la Señora Calley - 7º Grado
Tarea de Estudio de Películas 4 (fecha de entrega: semana 6)

Esta semana hemos visto "El Ciempiés Humano (primera parte)" en clase. Tu tarea es contestar a las sigientes preguntas sobre la película. Recuerda, las tareas deben ser entregadas antes del final de la semana 6. Recibirás una nota desde A+ (85% o más) hasta A (por debajo del 85%). 

Pregunta 1: Al comienzo de la película, las dos chicas son capturadas por un médico loco y sometidas a horribles torturas. Si estuvieras en su situación, ¿cómo te sentirías? Si tus sentimientos fueran un color, ¿qué color serían?

Pregunta 2: La película está calificada como "100% Exacta desde un punto de vista médico" . ¿Estás de acuerdo con esta afirmación? Da dos hechos científicos que apoyen tu opinión.

Pregunta 3: ¿Podrías idear una manera más efectiva de hacer un "ciempiés humano" sin que se reprodujeran las condiciones médicas de las víctimas de la película ? Adjunta un diagrama junto con varios hechos científicos que apoyen tu idea y una planificación escrita de tu proyecto.