﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeTe_Enc;
using SeTe_Dec;
using System.Threading;

namespace SeTe_Con
{
    class Program
    {
        static void Main(string[] args)
        {
            //Des
            Console.Title = "Secret Texts";
            Console.WriteLine("SeTe ready!");
            const string con = "Operation erfolgreich";
            bool re = true;
            do
            {
                Console.WriteLine("\nWas wollen Sie machen?\nv...Verschlüsseln\ne...Entschlüsseln\nb...beenden");
                string wtds = Console.ReadLine();
                if (wtds.Length==0)
                {
                    wtds = "a";
                }
                char wtd = wtds[0];
                if (wtd == 'v')
                {
                    Console.WriteLine("Geben Sie den Namen des Texts ein:");
                    string name = Console.ReadLine();
                    Console.WriteLine("Geben Sie den Text ein der Verschlüsselt werden soll:\n(Benutzten Sie die Enter-Taste nicht für die Formatierung des Texts!)");
                    string text = Console.ReadLine();
                    Enc.EncryptB(name, text);
                    Console.Clear();
                    Console.WriteLine(con);
                    Thread.Sleep(7000);
                    Console.Clear();
                }
                else if (wtd == 'e')
                {
                    Console.WriteLine("Geben Sie den Namen des Verschlüsselten Textes ein:");
                    string name = Console.ReadLine();
                    string text = Dec.Decrypt(name);
                    Console.Clear();
                    /*Console.Write("Ausgabe in: 5");
                    Thread.Sleep(1000);
                    Console.Write(" 4");
                    Thread.Sleep(1000);
                    Console.Write(" 3");
                    Thread.Sleep(1000);
                    Console.Write(" 2");
                    Thread.Sleep(1000);
                    Console.Write(" 1");
                    Thread.Sleep(1000);
                    Console.Clear();*/
                    Console.WriteLine(text);
                    Console.Write("\n\nDrücken Sie eine Taste");
                    Console.ReadKey();
                    Console.Clear();
                }
                else if (wtd == 'b')
                {
                    re = false;
                }
                else
                {
                    Console.Clear();
                    Console.Write("\nEingabe ist keine der oben genannten Möglichkeiten");
                    Console.ReadKey();
                    Console.Clear();
                }
            } while (re == true);
        }
    }
}
