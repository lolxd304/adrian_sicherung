﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Threading;

namespace WpfApplication1
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //---------------
            //------Alg------
            //---------------

            //Vars
            DriveInfo[] drives = new DriveInfo[26];
            bool[] wiederhohlen = new bool[100];

            //Zusästliche Usings
                //System.IO
                //System.Threading

            //---------------
            //-----Initi-----
            //---------------

            //ecf - Explorer Configuration Files

            //Drives
            wiederhohlen[0] = false;
            do
            {
                try
                {
                    drives = DriveInfo.GetDrives();
                }
                catch (IOException)
                {
                    MessageBox.Show("Explorer# war nicht in der Lage festzustellen, welche Laufwerke am Computer angeschlossen sind!", "Fehler!");
                    wiederhohlen[0] = true;
                }
                catch (UnauthorizedAccessException)
                {
                    MessageBox.Show("Sie verfügen nicht über die benötigten Berechtigungen um festzustellen welche Laufwerke am Computer angeschlossen sind!", "Fehler");
                    wiederhohlen[0]= true
                }
                Thread.Sleep(5000);
            } while (wiederhohlen[0] == true);
            
            
        }
    }
}
