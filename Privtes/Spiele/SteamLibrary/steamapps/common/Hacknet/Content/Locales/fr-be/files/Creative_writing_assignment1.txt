﻿﻿Mission 1 Écriture créative
Peter C.

John Stalvern attendait. Les lumières au-dessus de lui clignotaient et  Il
y avait des démons dans la base. Il ne les avait pas vus, mais les avait attendus depuis maintenant des années. Ses
avertissements n'avaient pas été entendus par Cernel Joson, et maintenant il était trop tard. Bien trop tard,
de toute façon.
John était un Space marine depuis quatorze ans. Quand il était jeune, il regardait les
vaisseaux spatiaux et dit à son père « Je veux aller dans ces vaisseaux papa. »
Et son père dit « Non ! Tu te feras TUÉ PAR LES DÉMONS »
Pendant un temps, il le crut. Un temps seulement. Mais maintenant qu'il se trouvait dans
la station spatiale de l'UAC, l'existence des démons ne lui avait jamais parue plus véridique.
« C'est Joson » laissa échapper la radio. « Tu dois combattre les démons ! »
Alors John saisit son fusil plasma et souffla un mur.
« IL VA NOUS TUER » dirent les démons
« Je vais lui tirer dessus » dit le cyber-démon et il tira les roquettes. John
riposta et fit feu sur lui avec son arme à plasma. Mais alors le plafond s'écroula et ils furent
piégés sans avoir pu le tuer.
« Non ! Je dois tuer les démons » s'écria-t-il
La radio dit « Non, John. TU es le démon »
Et John se transforma alors en zombie.