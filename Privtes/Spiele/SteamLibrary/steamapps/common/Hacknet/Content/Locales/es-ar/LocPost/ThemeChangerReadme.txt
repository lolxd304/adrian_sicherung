
-- Theme Changer Readme --

Este programa permite un intercambio rápido de archivos x-server desde fuentes locales o remotas.
Los archivos remotos son archivos válidos de un host remoto en la carpeta en la que te encuentres en un
ordenador conectado. Los archivos locales son archivos que se encuentran en sys o en home.
ThemeChanger permite al usuario seleccionar cualquiera de éstos, y descargará los contenidos en
x-server.sys en la carpeta sys y lo activará sin necesidad de un reinicio.
El programa también hará una copia de todos los archivos existentes así que todos los estilos serán preservados
para su uso futuro