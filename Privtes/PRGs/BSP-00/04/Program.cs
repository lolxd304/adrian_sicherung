﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04
{
    class Program
    {
        static void Main(string[] args)
        {
            string[,] länder =
            {
                {"imperium", "Altdorf"},
                {"österreich", "Wien"},
                {"tirol", "Innsbruck" },
                {"salzburg", "Salzburg"},
                {"wien", "Wien"}
            };

            do
            {
                Console.WriteLine("Geben Sie die Hauptstadt eines der 5 Länder ein:");
                string land = Console.ReadLine();
                if (land == länder[0, 0])
                {
                    Console.WriteLine(länder[0,1]);
                }
                else if (land == länder[1, 0])
                {
                    Console.WriteLine(länder[1, 1]);
                }
                else if (land == länder[2, 0])
                {
                    Console.WriteLine(länder[2, 1]);
                }
                else if (land == länder[3, 0])
                {
                    Console.WriteLine(länder[3, 1]);
                }
                else if (land == länder[4, 0])
                {
                    Console.WriteLine(länder[4, 1]);
                }
            } while (1 == 1);
            
        }
    }
}
