﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _01
{
    class Program
    {
        static void Main(string[] args)
        {
            const int constInt = 0;
            int varInt = 1;
            const double constDouble = 0.5;
            double varDouble = 0.6;
            const char constChar = 'a';
            char varChar = 'b';
            const string constString= "abc";
            string varString = "bcd";
            const bool constBool = false;
            bool varBool = true;
            Console.Title = "Daten";
            Console.WriteLine("Int-Werte:\n\nKonstant: {0}\nVeriabel: {1}", constInt, varInt);
            Thread.Sleep(5000);
            Console.Clear();
            Console.WriteLine("Double-Werte:\n\nKonstant: {0}\nVariabe: {1}", constDouble, varDouble);
            Thread.Sleep(5000);
            Console.Clear();
            Console.WriteLine("Char-Zeichen:\n\nKonstant: {0}\nVariabe: {1}", constChar, varChar);
            Thread.Sleep(5000);
            Console.Clear();
            Console.WriteLine("String-Zeichensätze:\n\nKonstant: {0}\nVariabe: {1}", constString, varString);
            Thread.Sleep(5000);
            Console.Clear();
            Console.WriteLine("Booooooooooooooooooooooooooooooooooooooooooool-Werte:\n\nKonstant: {0}\nVariabe: {1}", constBool, varBool);
            Thread.Sleep(5000);
            Console.Clear();
        }
    }
}
