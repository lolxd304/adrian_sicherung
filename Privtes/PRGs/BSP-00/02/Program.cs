﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] werte = new double[5];
            string[] schülername = new string[100];
            char[] vokale = { 'a', 'e', 'i', 'o', 'u' };

            double[,] kommazahlen = new double[2, 3];

            int[,] zahlen =
            {
                { 1, 2 },
                { 3, 4 },
                { 5, 6 },
                { 7, 8 }
            };

            //Werte: alles eingeben
            for(int i = 0; i<werte.Length; i++)
            {
                Console.WriteLine("Geben Sie die {0}. Kommazahl ein", i+1);
                werte[i] = Convert.ToDouble(Console.ReadLine());
                Console.Clear();
            }

            bool weiter = true;
            int ticker = 0;
            do
            {
                Console.WriteLine("Wollen Sie beenden?[+=ja,-=nein]");
                string weiters = Console.ReadLine();
                char weiterc = weiters[0];
                if (weiterc == '+')
                {
                    weiter = false;
                }
                else if (weiterc == '-')
                {
                    Console.WriteLine("Geben Sie den {0}. Schülernamen ein:", ticker + 1);
                    schülername[ticker] = Console.ReadLine();
                    ticker++;
                    Console.Clear();
                }
                else
                {
                    Console.WriteLine("Fehler!");
                    Console.ReadKey();
                    Console.Clear();
                }
            } while (weiter == true);
            
            //Ausg
            foreach (double i in werte)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("");

            for(int i = 0; i<=ticker; i++)
            {
                Console.WriteLine(schülername[i]);
            }

            Console.WriteLine("");

            foreach(char i in vokale)
            {
                Console.WriteLine(i);
            }

            //2dim array
            for (int i = 0; i < zahlen.GetLength(0); i ++)
            {
                for (int o = 0; i < zahlen.GetLength(1); o++)
                {
                    Console.Write("{0,5}", Convert.ToString(zahlen[i,o]));
                }
                Console.Write("\n");
            }


            Console.ReadKey();
        }
    }
}