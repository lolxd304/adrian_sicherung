
-- Theme Changer Leesmij --

Dit programma staat snel wisselen van x-server thema bestanden toe van lokale en remote verbonden bronnen.
Bestanden in de "Remote" rij worden gehost op de remote en zijn valdie thema bestanden in de huidige map op
een verbonden machine. Bestanden in de "Local" rij zijn lokale bestanden in de home of sys mappen.
ThemeChanger staat een gebruiker toe om er een te selecteren en zal dan automatisch zijn eigenschappen opslaan naar
x-server.sys in de system map en het systeem thema activeren zonder dat er een herstart nodig is.
Het programma zal ook automatisch bestaande thema's back-uppen zodat alle bekende stijlen bewaard blijven
voor toekomstig gebruik.