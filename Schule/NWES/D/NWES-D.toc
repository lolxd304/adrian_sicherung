\select@language {german}
\contentsline {part}{\numberline {I}Digitaltechnik}{5}
\contentsline {subsubsection}{\nonumberline Digital}{7}
\contentsline {paragraph}{\nonumberline Technische Beispiele}{7}
\contentsline {subsubsection}{\nonumberline Analog}{7}
\contentsline {paragraph}{\nonumberline Beispiele}{7}
\contentsline {subsubsection}{\nonumberline Schalter}{8}
\contentsline {chapter}{\numberline {1}Boolesche Algebra}{9}
\contentsline {subsection}{\numberline {1.0.1}Und $\land $}{9}
\contentsline {subsection}{\numberline {1.0.2}Oder $\lor $}{9}
\contentsline {subsection}{\numberline {1.0.3}Not $\lnot $}{9}
\contentsline {subsection}{\numberline {1.0.4}\ref {img_schalter_2schalter} als logischer Ausdruck}{10}
\contentsline {section}{\numberline {1.1}Rechenregeln}{10}
\contentsline {subsubsection}{\nonumberline Assiotativgestetz}{10}
\contentsline {subsubsection}{\nonumberline Kommutativgesetz}{10}
\contentsline {subsubsection}{\nonumberline Distributivgesetz}{10}
\contentsline {subsubsection}{\nonumberline De Morgan}{11}
\contentsline {subsubsection}{\nonumberline Negativ + Negativ}{11}
\contentsline {subsubsection}{\nonumberline Beispiele}{11}
\contentsline {section}{\numberline {1.2}Wahrheitstabelle und disjunktive Normalform}{12}
\contentsline {subsubsection}{\nonumberline Bsp.: Pumpensteuerung}{12}
\contentsline {paragraph}{\nonumberline Die Wahrheitstabelle}{12}
\contentsline {subsubsection}{\nonumberline PA:}{12}
\contentsline {subsubsection}{\nonumberline PB:}{12}
\contentsline {paragraph}{\nonumberline Beweis:}{12}
\contentsline {paragraph}{\nonumberline Wie kommt man zur DNF?}{12}
\contentsline {paragraph}{\nonumberline Kochrezept f\IeC {\"u}r DNF}{12}
\contentsline {section}{\numberline {1.3}Vereinfachung der DNF, KV-Diagramm}{13}
\contentsline {paragraph}{\nonumberline Kochrezept f\IeC {\"u}r KV-Diagramm}{13}
