﻿﻿
-- Readme Theme Changer --

Ce programme permet le remplacement à chaud de fichiers thème x-server à partir de sources connectées locales et distantes.
Les fichiers dans le rang "À distance" sont des fichiers thème valide hébergés à distance dans le dossier de navigation actuel sur une
machine connectée. Les fichiers dans le rang "Local" sont des fichiers thème hébergés localement dans le dossier sys ou accueil.
ThemeChanger permet à un utilisateur de sélectionner l'un de ceux-ci, et téléchargera automatiquement son contenu dans 
x-server.sys dans le dossier système, en plus d'activer le thème système sans nécessité d'être relancé.
Le programme récupèrera aussi automatiquement les thèmes existants pour que tous les styles connus soient préservés 
pour une utilisation future.