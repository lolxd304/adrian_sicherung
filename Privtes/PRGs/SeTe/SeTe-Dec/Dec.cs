﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SeTe_Dec
{
    public class Dec
    {
        public static string Decrypt(string name)
        {
            //Datein einlesen
            string path = "C:/SeTe/" + name + "/";
            string encts = File.ReadAllText(path + "et.etf");
            string iis = File.ReadAllText(path + "ii.iif");
            int tl1 = encts.Length;
            int tl2 = iis.Length;
            int tl;
            if (tl1 == tl2)
            {
                tl = tl1;
            }
            else
            {
                return "Fehler";
            }

            //Strings in Arrays
            char[] enctc = new char[tl];
            int[] iii = new int[tl];
            for(int i = 0; i<tl; i++)
            {
                enctc[i] = encts[i];
                iii[i] = iis[i];
            }

            //Chars in Ints
            int[] encti = new int[tl];
            for(int i = 0; i<tl; i++)
            {
                encti[i] = enctc[i];
            }

            //Zurrückveränderung
            int[] decti = new int[tl];
            for(int i = 0; i<tl; i++)
            {
                decti[i] = encti[i] - iii[i];
            }

            //Int in Char
            char[] dectc = new char[tl];
            for(int i = 0; i<tl; i++)
            {
                dectc[i] = Convert.ToChar(decti[i]);
            }

            //Char zu String
            string dects = " ";
            for(int i = 0; i<tl; i++)
            {
                if(i == 0)
                {
                    dects = Convert.ToString(decti[0]);
                }
                else
                {
                    dects = dects + Convert.ToString(decti[i]);
                }
            }

            return dects;
        }
    }
}