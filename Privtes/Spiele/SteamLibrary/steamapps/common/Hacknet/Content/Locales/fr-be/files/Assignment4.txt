﻿﻿Mlle Calley, Classe de 7e année
Étude de film devoir 4 (délai : semaine 6)

Cette semaine nous avons regardé « The Human Centipede (first sequence) » en classe. Maintenant vous devez répondre aux questions suivantes sur le film. Souvenez-vous, les devoirs doivent être rendus à la fin de la semaine 6. Vous recevrez une note allant de A+ (85 % et plus) à A (moins de 85 %) 

Question 1 : Au début du film, les deux filles sont capturées par un docteur fou qui leur inflige d'horribles tortures. Si vous étiez dans leur situation, comment vous sentiriez-vous, et si vos sentiments devaient être représentés par une couleur, quelle serait-elle ?

Question 2 : Le film se targue d'être « 100 % correct médicalement ». Êtes-vous d'accord avec cette affirmation ? Fournissez deux faits scientifiques pour soutenir votre réponse.

Question 3 : Pouvez-vous concevoir une méthode plus efficace de créer un « mille-pattes humain » qui ne subirait pas les problèmes médicaux des victimes du film ? Fournissez un diagramme et plusieurs faits scientifiques ainsi qu'une version écrite de votre plan.