﻿﻿Doc Design 4
[NB : Ce document de conception contient des détails qui n'ont pas été publiés à l'heure actuelle et qui concernent une sortie à venir. Veuillez ne pas les diffuser en dehors des réseaux de la société ! - Rick]

Secretary Simulator 2012
Slogan : « Des personnes qui vous ont offert un simulateur de véhicule utilitaire, un simulateur de jardinage, un simulateur d'épicier, voici Secretary Simulator 2012 : gérez des fichiers virtuels et répondez à des mémos virtuels ! Une expérience de bureau complète, et tout ceci dans le confort de votre domicile, depuis votre propre ordinateur ! C'est si intense que vous mettrez une cravate et irez vous chercher un café ! »

Fonctionnalités :
- Des feuilles de calcul virtuelles (Feuille de calcul mini-jeu)
- Des réunions de bureau virtuelles (arbre de dialogue, selon l'alignement du joueur)
- Des mémos virtuels (texte généré aléatoirement, sert aussi de mission tutorielle)
- Bureau virtuel (similaire à la décoration d'intérieur d'autres jeux, le joueur peut déplacer et arranger différents objets sur son bureau)
#NAME?
#NAME?

Notes : 
- Alignements : Le système d'alignement est basé sur une grille d'alignement en 2D similaire aux graphiques « d'alignement politique ». Les deux spectres sont Fainéant/Consciencieux et Loyal/Chaotique. L'alignement du joueur est déterminé par ses réponses dans les réunions
- Fins multiples : également déterminées par les réponses dans les réunions. 5 fins « normales » basées sur l'alignement & les choix pendant les missions (fin promotion, fin licenciement, fin fusion d'entreprises, fin merde sur le bureau des collègues de travail (chaotique/fainéant seulement) et fin promu au poste de PDG), 1 fin « spéciale » (décernée après avoir atteint 100 % au mini-jeu de feuille de calcul et trombone)
Mission q : 
À cause de la baisse de revenus du quatrième trimestre, nous avons été obligés de supprimer la populaire « mission 4 » et de la remplacer par un « simulateur d'écran de veille ».  Il s'agit simplement de l'image d'un chat qui tourne lentement et change de couleurs. [NB : Steve : Pouvons-nous remplacer cela avec l'un des fichiers du dossier "Pilotes d'imprimante" ?] [NB : Admin : Non, il n'y a pas de dossier pilotes d'imprimante. Le dossier pilotes d'imprimante est un mythe fréquemment propagé dans ce bureau et, honnêtement, je suis déçu que tu es cru à de tels mensonges, Steve. Je laisse ceci comme rappel pour tous les employés.]