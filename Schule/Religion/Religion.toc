\select@language {german}
\contentsline {chapter}{\numberline {1}Drogen}{5}
\contentsline {section}{\numberline {1.1}Brainstorming}{5}
\contentsline {section}{\numberline {1.2}Drogen-ABC}{5}
\contentsline {subsection}{\numberline {1.2.1}Orientierungspunkte f\IeC {\"u}r Drogen-ABC}{5}
\contentsline {subsection}{\numberline {1.2.2}Alkohol}{5}
\contentsline {subsubsection}{\nonumberline Herstellung}{5}
\contentsline {paragraph}{\nonumberline G\IeC {\"a}rung}{5}
\contentsline {paragraph}{\nonumberline Destilation}{5}
\contentsline {paragraph}{\nonumberline Weitere Information}{5}
\contentsline {subsubsection}{\nonumberline Wirkung}{5}
\contentsline {paragraph}{\nonumberline 0,2-0,5\IeC {\textperthousand }}{5}
\contentsline {paragraph}{\nonumberline ab 0,5\IeC {\textperthousand }}{5}
\contentsline {paragraph}{\nonumberline ab 0,8\IeC {\textperthousand }}{6}
\contentsline {paragraph}{\nonumberline 1-2\IeC {\textperthousand }}{6}
\contentsline {paragraph}{\nonumberline 2-3\IeC {\textperthousand }}{6}
\contentsline {paragraph}{\nonumberline 3-5\IeC {\textperthousand }}{6}
\contentsline {subsubsection}{\nonumberline Risiken und Gefahren}{6}
\contentsline {subsubsection}{\nonumberline Folgen f\IeC {\"u}r K\IeC {\"o}rper/Psyche/Soziales Umfeld}{6}
\contentsline {paragraph}{\nonumberline K\IeC {\"o}rperliche und Psychische Sch\IeC {\"a}den}{6}
\contentsline {paragraph}{\nonumberline Soziales Umfeld}{6}
\contentsline {subsection}{\numberline {1.2.3}Tabak}{6}
\contentsline {subsubsection}{\nonumberline Herstellung}{6}
\contentsline {subsubsection}{\nonumberline Wirkung}{6}
\contentsline {subsubsection}{\nonumberline Risiken und Gefahren}{7}
\contentsline {subsubsection}{\nonumberline Folge f\IeC {\"u}r K\IeC {\"o}rper/Psyche/Soziales Umfeld}{7}
\contentsline {subsection}{\numberline {1.2.4}Smart Drugs}{7}
\contentsline {subsubsection}{\nonumberline Natural drugs}{7}
\contentsline {paragraph}{\nonumberline Herstellung}{7}
\contentsline {paragraph}{\nonumberline Wirkung}{7}
\contentsline {paragraph}{\nonumberline Risiken und Gefahren}{8}
\contentsline {subsubsection}{\nonumberline Smart drugs}{8}
\contentsline {subsection}{\numberline {1.2.5}Alkohol in der Schwangerschaft}{8}
\contentsline {subsubsection}{\nonumberline Wirkung}{8}
\contentsline {subsubsection}{\nonumberline Risiken und Gefahren}{8}
\contentsline {subsection}{\numberline {1.2.6}Schlaf- und Beruhigungsmittel}{8}
\contentsline {subsubsection}{\nonumberline Wirkwei\IeC {\ss }e}{8}
\contentsline {subsubsection}{\nonumberline Risiken und Gefahren}{8}
