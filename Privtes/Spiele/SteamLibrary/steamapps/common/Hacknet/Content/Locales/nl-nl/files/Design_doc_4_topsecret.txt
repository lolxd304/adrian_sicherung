Design Doc 4
[NB: Dit design doc bevat nog-niet-openbaar-gemaakte details over een opkomende uitgave. Niet buiten bedrijfsnetwerken distribueren! -Rick]

Secretaris Simulator 2012
Slogan: "Van de makers van bedrijfsvoertuig simulator, tuinier simulator en groente simulator komt nu Secretaris Simulator 2012 - beheer virtuele bestanden, beantwoord virtuele memo's, - de volledige kantoor ervaring, alles van de comfort van je eigen computer! Het is zo intens dat je naar je stropdas en koffie zal rijken!"

Eigenschappen:
-Virtuele spreadsheets (spreadsheets minigame)
-Virtuele bedrijfsvergaderingen (dialoog-boom, dit is waar speler-opstelling op gebaseerd wordt)
-Virtuele memo's (willekeurig gegenereerde smaak-teksten, zijn ook de introductie-missie)
-Virtuele telefoon gesprekken (quicktime events)
-Virtual Basic

Notities:
-opstellingen: Het opstelling systeem is gebaseerd op een 2D opstellings raster - vergelijkbaar met "politieke liggings" grafieken. De twee spectra zijn Luilak/Ijverig en Rechtmatig/Chaotisch. De spelers opstelling wordt bepaald door hun antwoorden in vergaderingen.
-meerdere eindes: ook bepaald door antwoorden in vergaderingen. 5 "normale" eindes gebaseerd op opstelling & keuzes in missies (promotie einde, ontslagen einde, poep op collega's bureau einde (alleen chaotische luilak) en bevorderd tot directeur einde), 1 "speciaal" einde (gekregen na 100% voltooing van de paperclip en spreadsheet minigames)
Missie q:
Vanwege tegenvallende resultaten in het vierde kwartaal zijn we gedwongen om de populaire "missie 4" te vervangen met "screensaver simulator". Dit is slechts een afbeelding van een kat die langzaam ronddraaid en van kleur veranderd. [NB: Steve: Kunnen we dit vervangen met een van de bestanden in de "Printer Drivers" map?] [NB: Admin: Nee, er is gen printer driver map. De printer driver map is een vaak doorvertelde mythe en eerlijk gezegd; ik walg er van dat je in zulke leugens geloofd, Steve. Ik laat dit hier als een herinnering aan alle werknemers.]