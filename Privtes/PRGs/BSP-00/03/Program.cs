﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03
{
    class Program
    {
        static void Main(string[] args)
        {
            //Simonsicherheit
            Console.WriteLine("Dieses Programm ist nicht simonsicher!!");
            Console.ReadKey();
            Console.Clear();

            //Für Blinken Tasks nachschauen!!
            
            //Standart
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.Title = "Facebook";
            Console.Clear();

            //Arrays
            int[] alter = new int[10];
            string[] email = new string[10];
            string[] name = new string[10];

            //Eing
            int ticker = 0;
            bool weiter = true;

            do
            {
                Console.WriteLine("Geben Sie die Daten zum {0} Benutzer ein:", ticker + 1);
                Console.Write("Name: ");
                name[ticker] = Console.ReadLine();

                //Email
                bool coremail = false;
                do
                {
                    Console.Write("Email: ");
                    email[ticker] = Console.ReadLine();
                    coremail = InOrdnung.WahrheitEmail(email[ticker]);
                    if (coremail != true)
                    {
                        Console.WriteLine("Fehlerhafte Email!");
                    }
                } while (coremail==false);

                //Alter
                bool corage = false;
                do
                {
                    Console.Write("Alter: ");
                    alter[ticker] = Convert.ToInt32(Console.ReadLine());
                    if (corage==false)
                    {
                        Console.WriteLine("Fehlerhaftes Alter!");
                    }
                }while (corage==false);

                //Allgemeines
                ticker++;
                Console.WriteLine("\nWollen Sie weitere Benutzerdaten eingeben? [true = ja; false = nein]");
                weiter = Convert.ToBoolean(Console.ReadLine());
            } while (weiter==true);
            Console.Clear();

            //Ausg
            Console.WriteLine("{0,35}{1,40}{2,5}", "Name", "Email ", "Alter");
            for (int i = 0; i <= 80; i++)
            {
                Console.Write('.');
            }
            Console.WriteLine();
            for(int i = 0; i<ticker; i++)
            {
                Console.WriteLine("{0,35}{1,40}{2,5}", name[i], email[i], alter[i]);
            }
            Console.ReadKey();
            
        }
    }

    class InOrdnung
    {
        public static bool WahrheitEmail(string emailadresse)
        {
            //Vars
            bool at = false, pt = false;
            char[] eChar= new char[1000];
            int eCharTicker = 0;

            //emailadresse in char
            for(int i= 0; i < emailadresse.Length; i++)
            {
                eChar[i] = emailadresse[i];
                eCharTicker++;
            }

            //überprüfung
            for (int i = 0; i < eCharTicker; i++)
            {
                if (eChar[i] == '@')
                {
                    at = true;
                }
                else if (at == true && eChar[i] == '.')
                {
                    return true;
                }
            }

            //Allgemein
            return false;
        }

        public static bool WahrheitAlter(int alter)
        {
            if (alter>=14 || alter <= 100)
            {
                return true;
            }

            return false;
        }
    }

    class Eingabe
    {
        public static int Alter()
        {
            //Vars
            int alter=0;
            string alterInString;
            char[] alterInChar = new char[3];
            bool[] istZahl = { false, false, false, false };

            //Eing
            alterInString = Console.ReadLine();

            //Zeichenlänge
            if(alterInString.Length > 3)
            {
                return 101;
            }

            //In char
            for(int i = 0; i < 3; i++)
            {
                alterInChar[i] = alterInString[i];
            }
            
            for(int i = 0; i<3; i++)
            {
                if((int)alterInChar[i] > 47 && (int)alterInChar[i] < 58)
                {
                    istZahl[i] = true;
                }
            }

            if(istZahl[0] == true && istZahl[1] == true && istZahl[2])
            {
                istZahl[3] = true;
            }
            else
            {
                return 101;
            }

            if(istZahl[3] == true)
            {
                alter = Convert.ToInt32(alterInString);
            }

            return alter;
        }
    }
}
