﻿﻿Topic horodatage
------------------------------------------
Salut, 
Juste pour prévenir - j'ai désactivé l'horodatage du forum.

C'est uniquement pour des raisons de sécurité, ça devrait rendre toute procédure judiciaire beaucoup plus compliquée à mettre en place si les archives du forum se retrouvaient un jour compromises.
Bien que ça me fasse de la peine de l'enlever, je pense qu'il était ignoré la plupart du temps, et que ça vaut la peine de le désactiver si ça sécurise un peu plus l'environnement.
-Mods
------------------------------------------
>ça devrait rendre toute procédure judiciaire beaucoup plus compliquée à mettre en place

Hahaha, vous êtes toujours aussi paranos.
------------------------------------------
Même si je pense que la réaction soit un peu excessive, c'est bien que les modos pensent à ce genre de chose.
------------------------------------------
>paranos
Ça n'a rien de parano, anon, si tu veux rester invisible tu ne peux pas te permettre d'être suffisant.
------------------------------------------
Ouais mais quand même, l'horodatage ? Ça ne suffit pas que le forum soit totalement anonyme ?
Enfin bon, peu importe, perso ça me gêne pas, qui vérifie ça de toute manière ?
------------------------------------------
>qui vérifie ça de toute manière ?

C'est pratique de voir si un post/topic est récent ou pas - si une conversation est encore en cours, etc.
En même temps, cette communauté est suffisamment restreinte pour que ça n'ait pas tellement d'importance.
------------------------------------------
>Ça n'a rien de parano, anon, si tu veux rester invisible tu ne peux pas te permettre d'être suffisant.
Pas mieux
------------------------------------------