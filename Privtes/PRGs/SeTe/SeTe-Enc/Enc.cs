﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SeTe_Enc
{
    public class Enc
    {
        public static void Encrypt(string name, string text)
        {
            //Directory
            string dir = "C:/SeTe/" + name + "/";
            Directory.CreateDirectory(dir);
            

            //String in Char-Array
            int tl = text.Length;
            char[] chars = new char[tl];
            for (int i = 0; i < tl; i++)
            {
                chars[i] = text[i];
            }


            //Char in Int
            int[] ascii = new int[tl];
            for (int i = 0; i < tl; i++)
            {
                ascii[i] = Convert.ToInt32(chars[i]);
            }


            //Veränderung um zufällige Zahl
            Random rnd = new Random();
            char[] enctc = new char[tl];
            int[] rndia = new int[tl];
            for(int i = 0; i<tl; i++)
            {
                int rndi = rnd.Next(10);
                ascii[i] = ascii[i] + rndi;
                rndia[i] = rndi;
                enctc[i] = Convert.ToChar(ascii[i]);
            }


            //Informationen in Strings
            string iis = " ";
            string enct = " ";
            for(int i = 0; i<tl; i++)
            {
                if (i == 0)
                {
                    iis = Convert.ToString(rndia[0]);
                    enct = Convert.ToString(enctc[0]);
                }
                else
                {
                    iis = iis + Convert.ToString(rndia[i]);
                    enct = enct + Convert.ToString(enctc[i]);
                }
            }


            //Strings in Dateien
            File.WriteAllText(dir + "tt.etf", enct);
            File.WriteAllText(dir + "ii.iif", iis);
        }

        public static void EncryptB(string name, string text)
        {
            //Allgemein
            string oprts = text;
            int tl = oprts.Length;

            //Ordner
            string path = "C:/SeTe/" + name;
            Directory.CreateDirectory(path);

            //Sring in Char
            char[] oprtc = new char[tl];
            for(int i = 0; i<tl; i++)
            {
                oprtc[i] = oprts[i];
            }

            //Char in Int
            int[] oprti = new int[tl];
            for(int i = 0; i<tl; i++)
            {
                oprti[i] = Convert.ToInt32(oprtc[i]);
            }

            //Zeichen verändern
            Random rnd = new Random();
            int[] encti = new int[tl];
            int[] iii = new int[tl];
            for(int i = 0; i<tl; i++)
            {
                iii[i] = rnd.Next(10);
                encti[i] = oprti[i] + iii[i];
            }

            //Int in Char
            char[] enctc = new char[tl];
            for(int i = 0; i<tl; i++)
            {
                enctc[i] = Convert.ToChar(encti[i]);
            }

            //Char in String
            string encts = " ";
            for(int i = 0; i<tl; i++)
            {
                if (i == 0)
                {
                    encts = Convert.ToString(enctc[0]);
                }
                else
                {
                    encts = encts + Convert.ToString(enctc[i]);
                }
            }

            //Int in String
            string iis = " ";
            for(int i = 0; i<tl; i++)
            {
                if (i == 0)
                {
                    iis = Convert.ToString(iii[0]);
                }
                else
                {
                    iis = iis + Convert.ToString(iii[i]);
                }
            }

            //Strings in Dateien
            File.WriteAllText(path + "/et.etf", encts);
            File.WriteAllText(path + "/ii.iif", iis);
        }
    }
}