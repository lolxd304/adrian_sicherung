﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05
{
    class Program
    {
        static void Main(string[] args)
        {
            //-------------------------------
            //--------------Alg--------------
            //-------------------------------

            //Des
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.Clear();
            Console.Title = "No. 5";

            //Var
            int[] zahlen = new int[10000];
            int anzahlZahlen;
            Random rnd = new Random();
            int summe = 0;
            int[] anzahlWerte = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            double[] prozent = new double[11];


            //-------------------------------
            //------------Zahlen-------------
            //-------------------------------

            //Anzahl
            anzahlZahlen = rnd.Next(10000);
            //anzahlZahlen = 100;

            //Zahlen generieren
            for (int i = 0; i < anzahlZahlen; i++)
            {
                zahlen[i] = rnd.Next(5, 16);
            }

            //-------------------------------
            //------------Werte--------------
            //-------------------------------

            //Summe
            for (int i = 0; i < anzahlZahlen; i++)
            {
                summe += zahlen[i];
            }


            //----------Häufigkeiten---------
            //Absolute Häufigkeit
            for (int i = 0; i < anzahlZahlen; i++)
            {
                switch (zahlen[i])
                {
                    case 5:
                        anzahlWerte[0]++;
                        break;
                    case 6:
                        anzahlWerte[1]++;
                        break;
                    case 7:
                        anzahlWerte[2]++;
                        break;
                    case 8:
                        anzahlWerte[3]++;
                        break;
                    case 9:
                        anzahlWerte[4]++;
                        break;
                    case 10:
                        anzahlWerte[5]++;
                        break;
                    case 11:
                        anzahlWerte[6]++;
                        break;
                    case 12:
                        anzahlWerte[7]++;
                        break;
                    case 13:
                        anzahlWerte[8]++;
                        break;
                    case 14:
                        anzahlWerte[9]++;
                        break;
                    case 15:
                        anzahlWerte[10]++;
                        break;
                }
            }

            //Relative Häufigkeit
            for (int i = 0; i < 11; i++)
            {                prozent[i] = (Convert.ToDouble(anzahlWerte[i]) / Convert.ToDouble(anzahlZahlen)) *100;

            }

            //-------------------------------
            //-------------Ausg--------------
            //-------------------------------

            //Summe
            Console.WriteLine("Summe: " + summe);
            Console.WriteLine("\n\n\n\n\n\n");

            //-----Häufigkeit-----
            Console.WriteLine("{0,45}", "Häufigkeit");
            Console.WriteLine("\n");
            Console.WriteLine("{0,4}{1,38}{2,38}", "Wert", "Absolute Häufigkeit", "Relative Häufigkeit");


            for(int i = 0; i<80; i++)
            {
                Console.Write("-");
            }
            Console.WriteLine();

            for(int i = 5; i<16; i++)
            {
                Console.WriteLine("{0,4}{1,38}{2,38:F2}%", i, anzahlWerte[i - 5], prozent[i - 5]);
            }
            
            
            for(int i = 5; i<=15; i++)
            {
                Console.Write(i + ": ");
                for(int o = 0; o<prozent[i-5]; o++)
                {
                    Console.Write(Convert.ToChar(2));
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}