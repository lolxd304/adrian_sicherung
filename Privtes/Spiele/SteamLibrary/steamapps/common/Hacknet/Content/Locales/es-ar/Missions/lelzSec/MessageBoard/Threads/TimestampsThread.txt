﻿﻿Hilo de las marcas de tiempo
------------------------------------------
Eh, tíos, 
Noticia: he deshabilitado las marcas de tiempo en el tablón.

Esto es exclusivamente por razones de seguridad, de esta forma las demandas legales serán mucho más difíciles en caso de que nuestros archivos se vean comprometidos alguna vez.
Por mucho que me dé pena verlas desaparecer, creo que se las ignoraba de todas formas, y merece la pena deshabilitarlas para estar un poco más seguros.
-Mods
------------------------------------------
>las demandas legales serán mucho más difíciles 

Ja, ja, estáis acojonados de verdad.
------------------------------------------
Aunque creo que es algo excesivo, está guay que los mods estén preocupados por ese tipo de cosas.
------------------------------------------
>acojonados
No estamos acojonados anon, permanecer invisibles consiste en no ser nunca complacientes.
------------------------------------------
Ya, pero ¿las marcas de tiempo? ¿No basta con que el tablón sea totalmente anónimo?
Quiero decir, da igual, que en el fondo no me preocupa, pero ¿quién las comprueba de todas formas?
------------------------------------------
>pero ¿quién las comprueba de todas formas?

Es bastante conveniente ver de cuándo es un mensaje/hilo, si la conversación todavía sigue, etc.
Dicho esto, esta comunidad es tan pequeña que en el fondo eso da igual.
------------------------------------------
>No estamos acojonados anon, permanecer invisibles consiste en no ser nunca complacientes.
Esto
------------------------------------------