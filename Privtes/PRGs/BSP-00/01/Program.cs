﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01
{
    class Program
    {
        static void Main(string[] args)
        {
            string eing = "Bitte geben Sie {0} ein:";
            //Variablen (Wertetypen)
            int zahl;
            double wert;
            bool hatABS;
            char buchstabe;
            string name; //kein Wertetyp
            decimal geldbetrag;

            //Konstanten (Wertetypen)
            const int zahlC = 1;
            const double wertC = 5.7;
            const bool hatABSC = true;
            const char buchstabeC = 'z';
            const string nameC = "Alfred";
            const decimal geldbetragC = 15.89m; //m=deccimal

            //Eingabe der Variablen
            Console.WriteLine(eing, "eine Zahl");
            zahl = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(eing, "eine Kommazahl");
            wert = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Bitte geben Sie ein ob das Fahrzeug ABS hat [true, false]");
            hatABS = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine(eing, "einen Buchstaben");
            buchstabe = Convert.ToChar(Console.ReadLine());
            Console.WriteLine(eing, "einen Namen");
            name = Console.ReadLine();
            Console.WriteLine(eing, "einen Geldbetrg");
            geldbetrag = Convert.ToDecimal(Console.ReadLine());
            Console.Clear();

            //Ausgabe der Variablen
            Console.WriteLine("{0}", zahl);
            Console.WriteLine("{0:f3}", wert);
            Console.WriteLine("{0,10}{1,15}", buchstabe, hatABS);
            Console.WriteLine("{0,20}", name);
            Console.WriteLine("{0,20:f2}", geldbetrag);

            Console.WriteLine("\n{0}", zahlC);
            Console.WriteLine("{0:f3}", wertC);
            Console.WriteLine("{0,10}{1,15}", buchstabeC, hatABSC);
            Console.WriteLine("{0,20}", nameC);
            Console.WriteLine("{0,20:f2}", geldbetragC);

            Console.ReadKey();
        }       
    }
}
